var scr = document.getElementById('screen');
var context = scr.getContext("2d");

//vars for the fractal stuff
var zoom = 100;
var shiftX = 0;
var shiftY = 0;
var it = 70;//the number of iterations the algorithm will run

const MOVE_DISTANCE = 20;
const ZOOM_AMOUNT = 3;//how much the buttons zoom by
const ITERATION_DISTANCE = 10;//how much the buttons change the iterations by
const WIDTH = 500;
const HEIGHT = 500;
const MIN_ZOOM = 20;//minmum zoon so you can't zoom too far

function getPixels(){
  scr.height = HEIGHT;//document.body.offsetHeight;
  scr.width = WIDTH;//document.body.offsetWidth;
}


function drawFractal(){
  let r;
  let i;
  let cr;
  let ci;
  let nerR;
  let n;
  for(let x = 0; x < WIDTH; x++){
    for(let y = 0; y < HEIGHT; y++){
      cr = (x - WIDTH/2)/zoom + shiftX;
      ci = (y - HEIGHT/2)/zoom + shiftY;
      r = 0;
      i = 0;
      n = 0;
      while(n < it && (r*r + i*i) < 4){
        newR = cr + (r*r) - (i*i);
        i = ci + (2*r*i);
        r = newR;
        n++;
      }
      if(n == it){
        context.fillStyle = "#800";
      }
      else{
        n *= 255/it;
        n = Math.floor(n);
        context.fillStyle = "hsl("+(255-n)+",100%,"+ (25 + 25*(n/255)) + "%)";
      }
      context.fillRect(x,y,1,1);
    }
  }
}

//listener for keypresses
window.onkeydown = function(e){
  let redraw = true;
  let key = e.keyCode;
  switch(key){
    case 38://up arrow
    case 87://w
      shiftY -= MOVE_DISTANCE/zoom;
      updateY();
      break;
    case 40://down arrow
    case 83://s
      shiftY += MOVE_DISTANCE/zoom;
      updateY();
      break;
    case 37://left arrow
    case 65://a
      shiftX -= MOVE_DISTANCE/zoom;
      updateX();
      break;
    case 39://right arrow
    case 68://d
      shiftX += MOVE_DISTANCE/zoom;
      updateX();
      break;
    case 187://+/=
        zoom *= ZOOM_AMOUNT;
        updateZoom();
      break;
    case 189://-/_
      if(zoom > MIN_ZOOM){
        zoom /= ZOOM_AMOUNT;
        updateZoom();
      }
      else{
        redraw = false;
      }
      break;
    case 73://i
      it += ITERATION_DISTANCE;
      updateIt();
      break;
    case 79://o
      if(it - ITERATION_DISTANCE > 0){
        it -= ITERATION_DISTANCE;
        updateIt();
      }
      else{
        redraw = false;
      }
      break;
    //otherwise do nothing
    default:
      redraw = false;
      console.log(key);
      break;
  }

  if(redraw){
    drawFractal();
  }
}

function updateZoom(){
  document.getElementById("zoom").value = zoom;
}

function updateX(){
  document.getElementById("x").value = shiftX;
}

function updateY(){
  document.getElementById("y").value = shiftY;
}

function updateIt(){
  document.getElementById("it").value = it;
}

document.getElementById("set").addEventListener("click", function(){
  shiftY = parseFloat(document.getElementById("y").value);
  shiftX = parseFloat(document.getElementById("x").value);
  zoom = parseFloat(document.getElementById("zoom").value);
  it = parseFloat(document.getElementById("it").value);
  drawFractal();
});

getPixels();
drawFractal();
updateY();
updateX();
updateZoom();
updateIt();
